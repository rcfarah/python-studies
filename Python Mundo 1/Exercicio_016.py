# Desafio #016

print('Nesse programa irei mostrar a porção inteira de qualquer número real digitado.')
numero = float(input('Digite um número real qualquer: '))
print('A porção inteira do número {} é: {}'.format(numero, int(numero)))
